/**
 * @file StackException.h
 * @author Rahul Walse
 * @brief Header file to handle exceptions for Stack overflow and underflow conditions
 * @version 0.1
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */

// Header guard
#ifndef STACKEXCEPTION_H
#define STACKEXCEPTION_H

#include <exception>
#include <cstring>

class StackOverflowException : public std::exception
{
    private:
        // Data members
        char message[50];

    public:
        // Parameterized constructor
        StackOverflowException(const char* errorMessage)
        {
            strcpy(message, errorMessage);
        }

        // Override what() function
        const char* what() const override
        {
            return message;
        }
};

class StackUnderflowException : public std::exception
{
    private:
        // Data members
        char message[50];

    public:
        //Parameterized constructor
        StackUnderflowException(const char* errorMessage)
        {
            strcpy(message, errorMessage);
        }

        // Override what() function
        const char* what() const override
        {
            return message;
        }
};

#endif  //!STACKEXCEPTION_H