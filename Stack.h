/**
 * @file Stack.h
 * @author Rahul Walse
 * @brief Header file for implementing Stack data structure to push/pop object of type T
 * @version 0.1
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */

// Header guard
#ifndef STACK_H
#define STACK_H

#include <iostream>
#include "StackException.h"

template <typename T>
class Stack
{
    private:
        // Data members
        unsigned size;
        T* array;
        int top;

    public:
        // Special Member functions
        Stack();
        Stack(const unsigned&);
        ~Stack();

        // Member functions for operations on stack
        void Push(const T&);
        T Pop();
        const T& StackTop() const;
        inline bool IsFull() const;
        inline bool IsEmpty() const;
};


/* Implementation */

/* Special member functions */
// Implementation of default constructor
template <typename T>
Stack<T>::Stack() : size(5), array(new T[size]), top(-1)
{

}

// Implementation of parameterized constructor
template <typename T>
Stack<T>::Stack(const unsigned& size) : size(size), array(new T[size]), top(-1)
{

}

// Implementation of destructor
template <typename T>
Stack<T>::~Stack()
{
    //- Deallocate memory allocated and pointed by array
    if(array != nullptr)
        delete [] array;
}

/* Member functions */

// Implementation of function to push/store object of type T onto stack
template <typename T>
void Stack<T>::Push(const T& data)
{
    if(IsFull())
        throw StackOverflowException("ERROR: Stack is full!");
    else
        array[++top] = data;
}

// Implementation of function to pop/retrieve object of type T from stack
template <typename T>
T Stack<T>::Pop()
{
    if(IsEmpty())
        throw StackUnderflowException("ERROR: Stack is empty!");
    else
        return array[top--];
}

// Implementation of function to return object of type T at the top of stack
template <typename T>
const T& Stack<T>::StackTop() const
{
    if(IsEmpty())
        throw StackUnderflowException("ERROR: Stack is empty!");
    else
        return array[top];
}

// Implementation of function to check if the stack is full
template <typename T>
bool Stack<T>::IsFull() const
{
    return top == size - 1;
}

// Implementation of function to check if the stack is empty
template <typename T>
bool Stack<T>::IsEmpty() const
{
    return top == -1;
}

#endif  //!STACK_H