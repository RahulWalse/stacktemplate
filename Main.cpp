/**
 * @file Main.cpp
 * @author Rahul Walse
 * @brief Main file to test the operations on Stack of type T
 * @version 0.1
 * @date 2022-05-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <iostream>
#include "Stack.h"
#include "StackException.h"
#include "Student.h"

int main()
{
    std::cout << "\nPlease provide size of Student Stack: ";
    unsigned size{};
    std::cin >> size;

    Stack<Student> myStack(size);

    // Variables for menu
    bool stay = true;   int option = 0;

    // Variables to store Student details
    std::string studentName;    double percentage;

    while(stay)
    {
        // Display options
        std::cout << "\n1. Push Student object onto Stack\n2. Pop Student object from Stack";
        std::cout << "\n3. Object at Stack top\n4. Exit\nChoose an option: ";
        std::cin >> option;

        switch (option)
        {
            case 1: std::cout << "\nEnter student details:\n";
                    std::cout << "Name: ";          std::cin >> studentName;
                    std::cout << "Percentage: ";    std::cin >> percentage;
                    try
                    {
                        myStack.Push(Student(studentName, percentage));
                    }
                    catch(StackOverflowException error)
                    {
                        std::cerr << '\n' << error.what() << '\n';
                    }
                    break;

            case 2: try
                    {
                        std::cout << "\nPopped Student object: " << myStack.Pop() << std::endl;
                    }
                    catch(StackUnderflowException error)
                    {
                        std::cerr << '\n' << error.what() << '\n';
                    }
                    break;

            case 3: try
                    {
                        std::cout << "\nStudent object at top: " << myStack.StackTop() << std::endl;
                    }
                    catch(StackUnderflowException error)
                    {
                        std::cerr << '\n' << error.what() << '\n';
                    }
                    break;

            case 4: std::cout << "\nExiting the program...\n";
                    stay = false;
                    break;

            default:std::cout << "\nInvalid choice! Please choose from the options provided.\n";
                    break;
        }
    }
}