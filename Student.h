/**
 * @file Student.h
 * @author Rahul Walse
 * @brief Class template definition for student object to be stored as data part in node of singly linked list
 * @version 0.1
 * @date 2022-05-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */

// Header guard
#ifndef STUDENT_H
#define STUDENT_H

#include <string>

class Student
{
    private:
        // Data members
        unsigned rollNumber;
        std::string name;
        double percentage;
        static unsigned number;

    public:
        // Special member functions
        Student() = default;
        Student(const std::string&, const double&);
        ~Student() = default;

        // Accessor functions
        unsigned getRollNumber() const;
        std::string getName() const;
        double getPercentage() const;

        // Operator overloading
        Student& operator=(const Student&);
        
        // Friend function to overload '<<' operator
        friend std::ostream& operator<<(std::ostream&, const Student&);
};

#endif  //!STUDENT_h